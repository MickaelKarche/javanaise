package org.grp7.jvn;

import java.io.*;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;


public class JvnCoordImpl
        extends UnicastRemoteObject
        implements JvnRemoteCoord {

    private static JvnCoordImpl jc;

    /**
     * Compteur Objets présent dans le cache
     */
    private int currentObjectId;

    /**
     * Relations Identification - Noms jvnObjects
     */
    private HashMap<Integer, String> remoteServerObjectsId;

    /**
     * Objets stockés sur le cache
     */
    private HashMap<String, JvnObject> objectsCoordinator;

    /**
     * Objects avec un Lock en écriture / lecture
     */

    private HashMap<Integer, JvnRemoteServer> remoteObjectsWriter;
    private HashMap<Integer, ArrayList<JvnRemoteServer>> remoteObjectsReader;

    private static final long serialVersionUID = 1L;

    /**
     * Default constructor
     *
     * @throws JvnException
     **/
    public JvnCoordImpl() throws Exception {
        try (
            final FileInputStream  fileObjectsName = new FileInputStream ("objectsCoordinatorName.coor");
            ObjectInputStream  oosName = new ObjectInputStream (fileObjectsName);

            final FileInputStream  fileObjectsId = new FileInputStream ("objectsCoordinatorId.coor");
            ObjectInputStream  oosId = new ObjectInputStream (fileObjectsId);

            final FileInputStream  fileObjectsReader = new FileInputStream ("remoteObjectsReader.coor");
            ObjectInputStream  oosObjectReader = new ObjectInputStream (fileObjectsReader);

            final FileInputStream  fileObjectsWriter = new FileInputStream ("remoteObjectsWriter.coor");
            ObjectInputStream  oosObjectWriter = new ObjectInputStream (fileObjectsWriter);
        ) {

            this.objectsCoordinator = (HashMap<String, JvnObject>) oosName.readObject();
            this.remoteServerObjectsId = (HashMap<Integer, String>) oosId.readObject();
            this.remoteObjectsReader = (HashMap<Integer, ArrayList<JvnRemoteServer>>) oosObjectReader.readObject();
            this.remoteObjectsWriter = (HashMap<Integer, JvnRemoteServer>) oosObjectWriter.readObject();
            this.currentObjectId = objectsCoordinator.size();
            System.out.println("Cache restored " + currentObjectId);
            System.out.println("Coordinator object : " + objectsCoordinator.keySet());
            System.out.println("Object Reader list : " + remoteObjectsReader.keySet());
            System.out.println("Object Writer list : " + remoteObjectsWriter.keySet());
        } catch (IOException | ClassCastException e) {
            this.remoteServerObjectsId = new HashMap<>();
            this.objectsCoordinator = new HashMap<>();
            this.remoteObjectsWriter = new HashMap<>();
            this.remoteObjectsReader = new HashMap<>();
            this.currentObjectId = 0;
            System.out.println(objectsCoordinator.keySet());
            System.out.println(remoteObjectsReader.keySet());
            System.out.println(remoteObjectsWriter.keySet());
        }
    }

    /**
     *
     * @return the unique instance of the coordinator
     */
    public static JvnCoordImpl jvnGetCoord() {
        if (jc == null) {
            try {
                jc = new JvnCoordImpl();
            } catch (Exception e) {
                return null;
            }
        }
        return jc;
    }


    /**
     * Allocate a NEW JVN object id (usually allocated to a
     * newly created JVN object)
     *
     * @throws RemoteException,JvnException
     **/
    public synchronized int jvnGetObjectId()
            throws RemoteException, org.grp7.jvn.JvnException {
        currentObjectId++;
        System.out.println(" id : " + currentObjectId);
        return currentObjectId;
    }

    /**
     * Associate a symbolic name with a JVN object
     *
     * @param jon : the JVN object name
     * @param jo  : the JVN object
     * @param js  : the remote reference of the JVNServer
     * @throws RemoteException ,JvnException
     **/
    public synchronized void jvnRegisterObject(String jon, JvnObject jo, JvnRemoteServer js)
            throws RemoteException, org.grp7.jvn.JvnException {
        this.remoteServerObjectsId.put(currentObjectId, jon);
        this.objectsCoordinator.put(jon, jo);
        saveCoordinatorState();
    }

    /**
     * Get the reference of a JVN object managed by a given JVN server
     *
     * @param jon : the JVN object name
     * @param js  : the remote reference of the JVNServer
     * @throws RemoteException,JvnException
     **/
    public synchronized JvnObject jvnLookupObject(String jon, JvnRemoteServer js)
            throws RemoteException, org.grp7.jvn.JvnException {
        return this.objectsCoordinator.get(jon);
    }

    /**
     * Get a Read lock on a JVN object managed by a given JVN server
     *
     * @param joi : the JVN object identification
     * @param js  : the remote reference of the server
     * @return the current JVN object state
     * @throws RemoteException, JvnException
     **/
    public synchronized Serializable jvnLockRead(int joi, JvnRemoteServer js)
            throws RemoteException, JvnException, InterruptedException {
        JvnRemoteServer remoteServerWriter = this.remoteObjectsWriter.get(joi);
        ArrayList<JvnRemoteServer> remoteServerReader = this.remoteObjectsReader.get(joi);

        if (remoteServerReader == null) {
            remoteServerReader = new ArrayList<>();
        }

        if (remoteServerReader.contains(js))
            return new JvnException("Cet objet possede deja le verrou en lecture");

        if (remoteServerWriter != null) {
            if (remoteServerWriter.equals(js)) {
                throw new JvnException("Tentative d'acquisition d'un verrou en lecture mais un verrou en écriture vous est déjà attribué");
            }
            Serializable s = remoteServerWriter.jvnInvalidateWriterForReader(joi);
            this.remoteObjectsWriter.remove(joi);
            remoteServerReader.add(remoteServerWriter);
            remoteServerReader.add(js);
            this.remoteObjectsReader.put(joi, remoteServerReader);
            this.objectsCoordinator.put(this.remoteServerObjectsId.get(joi), (JvnObject) s);
            saveCoordinatorState();
            return s;
        }
        remoteServerReader.add(js);
        this.remoteObjectsReader.put(joi, remoteServerReader);
        saveCoordinatorState();
        return this.objectsCoordinator.get(this.remoteServerObjectsId.get(joi));
    }

    /**
     * Get a Write lock on a JVN object managed by a given JVN server
     *
     * @param joi : the JVN object identification
     * @param js  : the remote reference of the server
     * @return the current JVN object state
     * @throws RemoteException, JvnException
     **/
    public synchronized Serializable jvnLockWrite(int joi, JvnRemoteServer js)
            throws RemoteException, JvnException, InterruptedException {
        JvnRemoteServer remoteServerWriter = this.remoteObjectsWriter.get(joi);
        ArrayList<JvnRemoteServer> remoteServerReader = this.remoteObjectsReader.get(joi);

        if (remoteServerWriter != null) {
            if (remoteServerWriter.equals(js))
                return new JvnException("Cet objet possede deja le verrou en écriture");
            Serializable s = remoteServerWriter.jvnInvalidateWriter(joi);
            this.remoteObjectsWriter.put(joi, js);
            this.objectsCoordinator.put(this.remoteServerObjectsId.get(joi), (JvnObject) s);
            saveCoordinatorState();
            return s;
        } else if (remoteServerReader != null) {
            remoteServerReader.forEach(jvnRemoteServer -> {
                try {
                    if (!js.equals(jvnRemoteServer))
                        jvnRemoteServer.jvnInvalidateReader(joi);
                } catch (RemoteException | JvnException | InterruptedException e) {
                    e.printStackTrace();
                }
            });
            remoteServerReader.clear();
            this.remoteObjectsReader.put(joi, remoteServerReader);
            this.remoteObjectsWriter.put(joi, js);
            saveCoordinatorState();
            return this.objectsCoordinator.get(this.remoteServerObjectsId.get(joi));
        }
        this.remoteObjectsWriter.put(joi, js);
        saveCoordinatorState();
        return this.objectsCoordinator.get(this.remoteServerObjectsId.get(joi));
    }

    /**
     * A JVN server terminates
     *
     * @param js : the remote reference of the server
     * @throws RemoteException, JvnException
     **/
    public void jvnTerminate(JvnRemoteServer js)
            throws RemoteException, JvnException {
        List<Integer> writerObjectsToDelete = new ArrayList<>();
        this.remoteObjectsWriter.forEach((integer, jvnRemoteServer) -> {
            if (jvnRemoteServer.equals(js)) {
                writerObjectsToDelete.add(integer);
            }
        });

        for (Integer i : writerObjectsToDelete) {
            this.remoteObjectsWriter.remove(i);
        }

        this.remoteObjectsReader.forEach((integer, listJvnRemoteServer) -> {
            if (listJvnRemoteServer.contains(js)) {
                listJvnRemoteServer.remove(js);
                this.remoteObjectsReader.put(integer, listJvnRemoteServer);
            }
        });
        saveCoordinatorState();
        System.out.println("a Remote server has been terminated");
    }

    /**
     * Save the name map
     */
    private void saveCoordinatorState() {
        try (
                final FileOutputStream fileObjectsName = new FileOutputStream("objectsCoordinatorName.coor");
                ObjectOutputStream oosName = new ObjectOutputStream(fileObjectsName);

                final FileOutputStream fileObjectsId = new FileOutputStream("objectsCoordinatorId.coor");
                ObjectOutputStream oosId = new ObjectOutputStream(fileObjectsId);

                final FileOutputStream fileObjectsReader = new FileOutputStream("remoteObjectsReader.coor");
                ObjectOutputStream oosObjectReader = new ObjectOutputStream(fileObjectsReader);

                final FileOutputStream fileObjectsWriter = new FileOutputStream("remoteObjectsWriter.coor");
                ObjectOutputStream oosObjectWriter = new ObjectOutputStream(fileObjectsWriter);
        ) {
            oosName.writeObject(this.objectsCoordinator);
            oosName.flush();
            oosId.writeObject(this.remoteServerObjectsId);
            oosId.flush();
            oosObjectReader.writeObject(this.remoteObjectsReader);
            oosObjectReader.flush();
            oosObjectWriter.writeObject(this.remoteObjectsWriter);
            oosObjectWriter.flush();
        } catch (IOException e) {
            System.err.println("Unable to save object coordinator map");
            e.printStackTrace();
        }
    }

    public void JvnRemoveRightFromList(int joi, JvnRemoteServer js) throws RemoteException, JvnException{
        if (remoteObjectsWriter.containsKey(joi))
            remoteObjectsWriter.remove(joi);
        this.remoteObjectsReader.forEach((integer, listJvnRemoteServer) -> {
            if (listJvnRemoteServer.contains(js)) {
                listJvnRemoteServer.remove(js);
                this.remoteObjectsReader.put(integer, listJvnRemoteServer);
            }
        });
    }
}

 
