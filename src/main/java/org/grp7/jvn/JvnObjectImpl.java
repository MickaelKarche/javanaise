package org.grp7.jvn;

import java.io.Serializable;
import java.rmi.RemoteException;

public class JvnObjectImpl implements JvnObject {
    int id;
    Serializable object;
    boolean isWaiting;
    LOCKSTATE currentState;

    public JvnObjectImpl(Serializable serializabledObject, int id) {
        this.object = serializabledObject;
        this.id = id;
        currentState = LOCKSTATE.NL;
        isWaiting = false;
    }

    @Override
    public void jvnLockRead() throws JvnException, RemoteException, InterruptedException {
        System.out.println("LockRead");
        System.out.println("etat: " + currentState);
        switch (currentState) {
            case NL:
                object = ((JvnObject) JvnServerImpl.jvnGetServer().jvnLockRead(id)).jvnGetSharedObject();
                currentState = LOCKSTATE.R;
                break;
            case WC:
//                object = ((JvnObject) JvnServerImpl.jvnGetServer().jvnLockRead(id)).jvnGetSharedObject();
                currentState = LOCKSTATE.RWC;
                break;
            case RC:
                currentState = LOCKSTATE.R;
                break;
        }
    }

    @Override
    public void jvnLockWrite() throws JvnException, RemoteException, InterruptedException {
        System.out.println("LockWrite");
        System.out.println("etat: " + currentState);
        switch (currentState) {
            case NL:
            case RC:
                object = ((JvnObject) JvnServerImpl.jvnGetServer().jvnLockWrite(id)).jvnGetSharedObject();
                currentState = LOCKSTATE.W;
                break;
            case RWC:
            case WC:
                currentState = LOCKSTATE.W;
                break;
        }
    }

    @Override
    public synchronized void jvnUnLock() throws JvnException {
//        if (isWaiting)
        System.out.println("Unlock");
        notify();
        switch (currentState) {
            case WC:
            case W:
            case RWC:
                currentState = LOCKSTATE.WC;
                break;
            case RC:
            case R:
                currentState = LOCKSTATE.RC;
                break;
            //default:
            //    throw new JvnException("Liberation d'un verrou alors que aucun verrou n'est pris");
        }
        System.out.println("etat final: " + currentState);
    }

    @Override
    public int jvnGetObjectId() throws JvnException {
        return id;
    }

    @Override
    public Serializable jvnGetSharedObject() throws JvnException {
        return object;
    }

    @Override
    public synchronized void jvnInvalidateReader() throws JvnException, InterruptedException {
        System.out.println("InvalidateReader");
        switch (currentState) {
            case RC:
                currentState = LOCKSTATE.NL;
                break;
            case RWC:
            case R:
                syncCall();
                currentState = LOCKSTATE.NL;
                break;
            default:
                throw new JvnException("Try to invalidate reader but the current state is:" + currentState);
        }
    }

    @Override
    public synchronized Serializable jvnInvalidateWriter() throws JvnException, InterruptedException {
        System.out.println("InvalidateWriter");
        switch (currentState) {
            case WC:
                currentState = LOCKSTATE.NL;
                return this;
            case RWC:
                currentState = LOCKSTATE.R;
                return this;
            case W:
                syncCall();
                currentState = LOCKSTATE.NL;
                return this;
            default:
                throw new JvnException("Try to invalidate writer but the current state is:" + currentState);
        }
    }

    @Override
    public synchronized Serializable jvnInvalidateWriterForReader() throws JvnException, InterruptedException {
        System.out.println("InvalidateRaderforWriter");
        switch (currentState) {
            case W:
                syncCall();
                currentState = LOCKSTATE.RC;
                return this;
            case WC:
                currentState = LOCKSTATE.RC;
                return this;
            case RWC:
                currentState = LOCKSTATE.R;
                return this;
            default:
                throw new JvnException("Try to invalidate writer for reader but the current state is:" + currentState);
        }
    }

    @Override
    public void syncCall() {
        try {
            wait();
        } catch (InterruptedException e) {
            System.out.println(e);
        }
    }
}
