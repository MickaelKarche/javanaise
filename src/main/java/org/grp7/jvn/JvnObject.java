/***
 * JAVANAISE API
 * Contact: 
 *
 * Authors: 
 */

package org.grp7.jvn;

import java.io.Serializable;
import java.rmi.RemoteException;

/**
 * Interface of a JVN object. 
 * A JVN object is used to acquire read/write locks to access a given shared object
 */


public interface JvnObject extends Serializable {
	/* A JvnObject should be serializable in order to be able to transfer 
       a reference to a JVN object remotely */

	/**
	* Get a Read lock on the shared object 
	* @throws JvnException
	**/
	public void jvnLockRead()
            throws org.grp7.jvn.JvnException, RemoteException, InterruptedException;

	/**
	* Get a Write lock on the object 
	* @throws JvnException
	**/
	public void jvnLockWrite()
			throws org.grp7.jvn.JvnException, RemoteException, InterruptedException;

	/**
	* Unlock  the object 
	* @throws JvnException
	**/
	public void jvnUnLock()
	throws org.grp7.jvn.JvnException;
	
	
	/**
	* Get the object identification
	* @throws JvnException
	**/
	public int jvnGetObjectId()
	throws org.grp7.jvn.JvnException;
	
	/**
	* Get the shared object associated to this JvnObject
	* @throws JvnException
	**/
	public Serializable jvnGetSharedObject()
	throws org.grp7.jvn.JvnException;
	
	
	/**
	* Invalidate the Read lock of the JVN object 
	* @throws JvnException
	**/
  public void jvnInvalidateReader()
		  throws org.grp7.jvn.JvnException, InterruptedException;
	    
	/**
	* Invalidate the Write lock of the JVN object  
	* @return the current JVN object state
	* @throws JvnException
	**/
  public Serializable jvnInvalidateWriter()
          throws org.grp7.jvn.JvnException, InterruptedException;
	
	/**
	* Reduce the Write lock of the JVN object 
	* @return the current JVN object state
	* @throws JvnException
	**/
   public Serializable jvnInvalidateWriterForReader()
           throws org.grp7.jvn.JvnException, InterruptedException;

   public void syncCall();

}
