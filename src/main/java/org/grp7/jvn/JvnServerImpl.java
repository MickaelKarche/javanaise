package org.grp7.jvn;

import java.rmi.AccessException;
import java.rmi.ConnectException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.io.*;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;


public class JvnServerImpl
        extends UnicastRemoteObject
        implements JvnLocalServer, JvnRemoteServer {

    private final Map<Integer, JvnObject> cachedObject = new HashMap<>();
    private final Map<Integer, Integer> recentlyUsed = new HashMap<>();
    private int maxCachedSize = 3;
    private static JvnRemoteCoord coordinator;


    /**
     *
     */
    private static final long serialVersionUID = 1L;
    // A JVN server is managed as a singleton
    private static JvnServerImpl js = null;

    /**
     * Default constructor
     *
     * @throws JvnException
     **/
    private JvnServerImpl() throws Exception {
        super();
        Registry registry = LocateRegistry.getRegistry(1099);
        coordinator = (JvnRemoteCoord) registry.lookup("JvnCoordinator");
    }


    public void setMaxCachedSize(int maxCachedSize) {
        this.maxCachedSize = maxCachedSize;
    }

    /**
     * Static method allowing an application to get a reference to
     * a JVN server instance
     *
     * @throws JvnException
     **/
    public static JvnServerImpl jvnGetServer() {
        if (js == null) {
            try {
                js = new JvnServerImpl();
            } catch (Exception e) {
                return null;
            }
        }
        return js;
    }

    /**
     * The JVN service is not used anymore
     *
     * @throws JvnException
     **/
    public void jvnTerminate()
            throws org.grp7.jvn.JvnException, RemoteException {

            try {
                coordinator.jvnTerminate(this);
            } catch (ConnectException e) {
                coordConnection();
                coordinator.jvnTerminate(this);
            }catch (RemoteException e) {
                System.out.println("Termination problem : " + e.getMessage());
                e.printStackTrace();
            }

    }

    /**
     * creation of a JVN object
     *
     * @param o : the JVN object state
     * @throws JvnException
     **/
    public JvnObject jvnCreateObject(Serializable o)
            throws org.grp7.jvn.JvnException, RemoteException {
            try {
                int id = coordinator.jvnGetObjectId();
                JvnObject jvnObject = new JvnObjectImpl(o, id);
                checkIfMaxCachedSizeReachedBeforeAdd(id, jvnObject);
                this.cachedObject.put(id, jvnObject);
                System.out.println(cachedObject);
                return jvnObject;
            } catch (ConnectException e) {
                coordConnection();
            } catch (RemoteException e) {
                System.out.println("Object creation failed : " + e.getMessage());
                e.printStackTrace();
            }
        return null;

    }

    /**
     * Associate a symbolic name with a JVN object
     *
     * @param jon : the JVN object name
     * @param jo  : the JVN object
     * @throws JvnException
     **/
    public void jvnRegisterObject(String jon, JvnObject jo)
            throws org.grp7.jvn.JvnException, RemoteException {

        try {
            coordinator.jvnRegisterObject(jon, jo, js);
        } catch (ConnectException e) {
            coordConnection();

        } catch (RemoteException e) {
            System.out.println("Object registering failed : " + e.getMessage());
            e.printStackTrace();
        }

    }

    /**
     * Provide the reference of a JVN object beeing given its symbolic name
     *
     * @param jon : the JVN object name
     * @return the JVN object
     * @throws JvnException
     **/
    public JvnObject jvnLookupObject(String jon)
            throws org.grp7.jvn.JvnException, RemoteException {
        int count = 0;
        int maxTries = 3;
        while(true) {
            try {
                JvnObject jo = coordinator.jvnLookupObject(jon, js);
                if (jo != null) {
                    checkIfMaxCachedSizeReachedBeforeAdd(jo.jvnGetObjectId(), jo);
                    this.cachedObject.put(jo.jvnGetObjectId(), jo);
                    System.out.println(cachedObject);
                }
                return jo;
            } catch (ConnectException e) {
                coordConnection();
                if (++count == maxTries)  {
                    throw new JvnException("Can't reconnect to server!");
                }
            } catch (RemoteException e) {
                System.out.println("Can't find the object : " + e.getMessage());
                e.printStackTrace();
                return null;
            }
        }

    }

    /**
     * Get a Read lock on a JVN object
     *
     * @param joi : the JVN object identification
     * @return the current JVN object state
     * @throws JvnException
     **/
    public Serializable jvnLockRead(int joi)
            throws JvnException, RemoteException, InterruptedException {

        int count = 0;
        int maxTries = 3;
        while(true) {
            try {
                Serializable toReturn = coordinator.jvnLockRead(joi, js);
                updateRecentlyUser(joi, ((JvnObject)toReturn));
                return toReturn;
            } catch ( ConnectException e) {
                System.out.println("Connection lost, try to reconnected");
                coordConnection();
                if (++count == maxTries)  {
                    throw new JvnException("Can't reconnect to server!");
                }
            } catch (RemoteException | InterruptedException e) {
                System.out.println("Can't have read lock on the object : " + e.getMessage());
                e.printStackTrace();
                return null;
            }
        }
    }

    /**
     * Get a Write lock on a JVN object
     *
     * @param joi : the JVN object identification
     * @return the current JVN object state
     * @throws JvnException
     **/
    public Serializable jvnLockWrite(int joi)
            throws JvnException, RemoteException, InterruptedException {
        int count = 0;
        int maxTries = 3;
        while(true) {
            try {
                Serializable toReturn = coordinator.jvnLockWrite(joi, js);
                updateRecentlyUser(joi, ((JvnObject)toReturn));
                return toReturn;
            } catch (ConnectException e) {
                System.out.println("Connection lost, try to reconnected");
                coordConnection();
                if (++count == maxTries)  {
                    throw new JvnException("Can't reconnect to server!");
                }
            } catch (RemoteException | InterruptedException e) {
                System.out.println("Can't have write lock on the object : : " + e.getMessage());
                e.printStackTrace();
                return null;
            }
        }
    }


    /**
     * Invalidate the Read lock of the JVN object identified by id
     * called by the JvnCoord
     *
     * @param joi : the JVN object id
     * @return void
     * @throws RemoteException,JvnException
     **/
    public synchronized void jvnInvalidateReader(int joi)
            throws RemoteException, org.grp7.jvn.JvnException, InterruptedException {
        JvnObject object = this.cachedObject.get(joi);
        object.jvnInvalidateReader();
    }


    /**
     * Invalidate the Write lock of the JVN object identified by id
     *
     * @param joi : the JVN object id
     * @return the current JVN object state
     * @throws RemoteException,JvnException
     **/
    public synchronized Serializable jvnInvalidateWriter(int joi)
            throws RemoteException, org.grp7.jvn.JvnException, InterruptedException {
        JvnObject object = this.cachedObject.get(joi);
        return object.jvnInvalidateWriter();
    }

    ;

    /**
     * Reduce the Write lock of the JVN object identified by id
     *
     * @param joi : the JVN object id
     * @return the current JVN object state
     * @throws RemoteException,JvnException
     **/
    public synchronized Serializable jvnInvalidateWriterForReader(int joi)
            throws RemoteException, org.grp7.jvn.JvnException, InterruptedException {
        JvnObject object = this.cachedObject.get(joi);
        return object.jvnInvalidateWriterForReader();
    }

    /**
     * Change order of Object in recentlyUsed Map with LRU strategy
     * @param joi
     */
    public void updateRecentlyUser(int joi, JvnObject jvnObject) {
        checkIfMaxCachedSizeReachedBeforeAdd(joi,jvnObject);
        int currentValue = recentlyUsed.get(joi);
        recentlyUsed.forEach((key, value) -> {
            if (value > currentValue) {
                recentlyUsed.replace(key, value - 1);
            }
        });
        recentlyUsed.replace(joi, recentlyUsed.size());
        System.out.println("After: ");
        System.out.println(recentlyUsed);
    }

    /**
     * check if the max object cached size is reached before adding a new object in cached
     * if it's reached it will remove the least recently used object from cache and add the new one
     * @param joi
     */
    public void checkIfMaxCachedSizeReachedBeforeAdd(int joi,JvnObject jvnObject) {
        System.out.println("Before: ");
        System.out.println("recentlyUsed: "+recentlyUsed);
        System.out.println("cachedObject: "+cachedObject);
        try{
            if (recentlyUsed.size() == maxCachedSize) {
                Iterator<Integer> it = recentlyUsed.keySet().iterator();
                while (it.hasNext()) {
                    Integer key = it.next();
                    Integer value = recentlyUsed.get(key);
                    if(value == 1) {
                        it.remove();
                        cachedObject.remove(key);
                        coordinator.JvnRemoveRightFromList(key, js);
                    }else{
                        recentlyUsed.replace(key, value - 1);
                    }
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        if(!recentlyUsed.containsKey(joi)){
            recentlyUsed.put(joi, recentlyUsed.size() + 1);
            cachedObject.put(joi, jvnObject);
            System.out.println("After: ");
            System.out.println("recentlyUsed: "+recentlyUsed);
            System.out.println("cachedObject: "+cachedObject);
        }
    }

    private void coordConnection() {
        try {
            Registry registry = LocateRegistry.getRegistry(1099);
            coordinator = (JvnRemoteCoord) registry.lookup("JvnCoordinator");
        } catch (RemoteException | NotBoundException e){
            System.out.println("Client error : " + e.getMessage());
        }
    }
}

 
