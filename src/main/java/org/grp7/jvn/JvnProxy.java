package org.grp7.jvn;

import java.io.Serializable;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.rmi.RemoteException;

public class JvnProxy implements InvocationHandler {
    private JvnObject jo;

    private JvnProxy(Object obj) {
        this.jo = (JvnObject) obj;
    }

    public static Object newInstance(String name, Object object, Integer maxCacheSize) throws JvnException, RemoteException {
        // initialize JVN
        JvnServerImpl js = JvnServerImpl.jvnGetServer();
        js.setMaxCachedSize(maxCacheSize);

        Runtime.getRuntime().addShutdownHook(new Thread(() -> {
            try {
                js.jvnTerminate();
            } catch (JvnException | RemoteException e) {
                e.printStackTrace();
            }
        }));

        // look up the IRC object in the JVN server
        // if not found, create it, and register it in the JVN server
        JvnObject jo = js.jvnLookupObject(name);

        if (jo == null) {
            jo = js.jvnCreateObject((Serializable) object);
            // after creation, I have a write lock on the object
            jo.jvnUnLock();
            js.jvnRegisterObject(name, jo);
        }

        return java.lang.reflect.Proxy.newProxyInstance(
                object.getClass().getClassLoader(),
                object.getClass().getInterfaces(),
                new JvnProxy(jo));
    }

    public Object invoke(Object proxy, Method m, Object args[]) {
        Object result = null;
        try {
            if (m.isAnnotationPresent(JvnObjectAnnotation.class)) {
                if (m.getAnnotation(JvnObjectAnnotation.class).lock().equals("Read")) {
                    jo.jvnLockRead();
                } else if (m.getAnnotation(JvnObjectAnnotation.class).lock().equals("Write")) {
                    jo.jvnLockWrite();
                }
                result = m.invoke(jo.jvnGetSharedObject(), args);
                jo.jvnUnLock();
                return result;
            }
        }catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }
}
