package org.grp7.jvn;

public enum LOCKSTATE {
    NL, RC, WC, R, W, RWC
}
