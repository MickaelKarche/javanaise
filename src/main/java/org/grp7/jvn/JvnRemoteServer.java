/***
 * JAVANAISE API
 * JvnRemoteServer interface
 * Defines the remote interface provided by a JVN server 
 * This interface is intended to be invoked by the Javanaise coordinator 
 * Contact: 
 *
 * Authors: 
 */

package org.grp7.jvn;

import java.io.Serializable;
import java.rmi.Remote;
import java.rmi.RemoteException;


/**
 * Remote interface of a JVN server (used by a remote JvnCoord) 
 */

public interface JvnRemoteServer extends Remote {
	    
	/**
	* Invalidate the Read lock of a JVN object 
	* @param joi : the JVN object id
	* @throws RemoteException,JvnException
	**/
  public void jvnInvalidateReader(int joi)
		  throws RemoteException, org.grp7.jvn.JvnException, InterruptedException;
	    
	/**
	* Invalidate the Write lock of a JVN object 
	* @param joi : the JVN object id
	* @return the current JVN object state 
	* @throws RemoteException,JvnException
	**/
        public Serializable jvnInvalidateWriter(int joi)
				throws RemoteException, org.grp7.jvn.JvnException, InterruptedException;
	
	/**
	* Reduce the Write lock of a JVN object 
	* @param joi : the JVN object id
	* @return the current JVN object state
	* @throws RemoteException,JvnException
	**/
   public Serializable jvnInvalidateWriterForReader(int joi)
		   throws RemoteException, org.grp7.jvn.JvnException, InterruptedException;

}

 
