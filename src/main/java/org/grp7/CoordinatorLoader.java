package org.grp7;


import org.grp7.jvn.JvnCoordImpl;
import org.grp7.jvn.JvnRemoteCoord;

import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

public class CoordinatorLoader {
    public static void main(String[] argv) {
        JvnRemoteCoord coordinator = null;
        try {
            coordinator = JvnCoordImpl.jvnGetCoord();
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            LocateRegistry.createRegistry(1099);
        } catch (RemoteException e) {
            e.printStackTrace();
        }

        try {
            Registry registry = LocateRegistry.getRegistry(1099);
            registry.rebind("JvnCoordinator", coordinator);
            System.out.println("Coordinator started!");
        } catch (Exception e) {
            e.printStackTrace();
            System.exit(1);
        }

    }
}
