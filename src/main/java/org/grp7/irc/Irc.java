/***
 * Irc class : simple implementation of a chat using JAVANAISE 
 * Contact: 
 *
 * Authors: 
 */

package org.grp7.irc;


import org.grp7.jvn.JvnException;
import org.grp7.jvn.JvnProxy;
import org.grp7.jvn.JvnServerImpl;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;


public class Irc {
    public TextArea text;
    public TextField data;
    Frame frame;
    ISentence sentence;


    /**
     * main method
     * create a JVN object nammed IRC for representing the Chat application
     **/
    public static void main(String argv[]) {
        try {
            if(argv.length == 2 ){
                try {
                    int arg = Integer.parseInt(argv[1]);
                    // initialize JVN
                    JvnServerImpl js = JvnServerImpl.jvnGetServer();

                    // create the graphical part of the Chat application
                    ISentence proxy = (ISentence) JvnProxy.newInstance("IRC",new Sentence(),arg);
                    Irc irc = new Irc(proxy);
                    if(argv.length != 0 && argv[0].equals("1"))
                        StressTest.lockStress(irc);

                } catch (NumberFormatException nfe) {
                    System.out.println("L'application doit avoir deux arguments:\nun nombre -> soit 0 pour desactiver le stresstest sinon 1\nun nombre -> la taille du cache.");
                }
            }else{
                System.out.println("L'application doit avoir deux arguments:\nun nombre -> soit 0 pour desactiver le stresstest sinon 1\nun nombre -> la taille du cache.");
            }

        } catch (Exception e) {
            System.out.println("IRC problem : " + e.getMessage());
        }
    }

    /**
     * IRC Constructor
     *
     * @param sentence the Interface of a sentence representing the Chat
     **/
    public Irc(ISentence sentence) {
        this.sentence = sentence;
        frame = new Frame();
        frame.addWindowListener(new MyWindowListener());
        frame.setLayout(new GridLayout(1, 1));
        text = new TextArea(10, 60);
        text.setEditable(false);
        text.setForeground(Color.red);
        frame.add(text);
        data = new TextField(40);
        frame.add(data);
        Button read_button = new Button("read");
        read_button.addActionListener(new readListener(this));
        frame.add(read_button);
        Button write_button = new Button("write");
        write_button.addActionListener(new writeListener(this));
        frame.add(write_button);
        frame.setSize(545, 201);
        text.setBackground(Color.black);
        frame.setVisible(true);
    }

    static class MyWindowListener extends WindowAdapter {
        public void windowClosing(WindowEvent e) {
            System.exit(0);
        }
    }
}


/**
 * Internal class to manage user events (read) on the CHAT application
 **/
class readListener implements ActionListener {
    Irc irc;

    public readListener(Irc i) {
        irc = i;
    }

    /**
     * Management of user events
     **/
    public void actionPerformed(ActionEvent e) {
        // invoke the method
        String s = this.irc.sentence.read();

        // display the read value
        irc.data.setText(s);
        irc.text.append(s + "\n");
    }
}

/**
 * Internal class to manage user events (write) on the CHAT application
 **/
class writeListener implements ActionListener {
    Irc irc;

    public writeListener(Irc i) {
        irc = i;
    }

    /**
     * Management of user events
     **/
    public void actionPerformed(ActionEvent e) {
        // get the value to be written from the buffer
        String s = irc.data.getText();

        // invoke the method
        this.irc.sentence.write(s);

    }
}



