/***
 * Sentence class : used for keeping the text exchanged between users
 * during a chat application
 * Contact: 
 *
 * Authors: 
 */
package org.grp7.irc;

public class Sentence implements java.io.Serializable, ISentence {
    /**
     *
     */
    private static final long serialVersionUID = 1L;
    String data;

    public Sentence() {
        data = "";
    }

    public void write(String text) {
        data = text;
    }

    public String read() {
        return data;
    }

}
