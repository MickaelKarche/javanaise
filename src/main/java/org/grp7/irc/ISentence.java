package org.grp7.irc;

import org.grp7.jvn.JvnObjectAnnotation;

import java.io.Serializable;

public interface ISentence extends Serializable {

    @JvnObjectAnnotation(lock = "Write")
    void write(String text);

    @JvnObjectAnnotation(lock = "Read")
    String read();

}
