package org.grp7.irc;

import java.util.Random;

public class StressTest {
    private static final Random rand = new Random();
    private static final int min = 10;
    private static final int max = 50;
    private static final int iter = rand.nextInt(max-min + 1) + min;
    private static boolean changed = rand.nextBoolean();
    private static final String testSentence = "TEST";

    public static void lockStress(Irc irc) throws InterruptedException {
        String s;
        for(int i=0; i < iter; i++) {
            if (changed) {
                irc.sentence.write(testSentence);
                s = irc.data.getText();
            } else {
                s = irc.sentence.read();
                irc.data.setText(s);
                irc.text.append(s + "\n");
            }
            changed = rand.nextBoolean();
            Thread.sleep(100);
        }
        s = "Terminé !";
        irc.data.setText(s);
        irc.text.append(s + "\n");
    }
}
