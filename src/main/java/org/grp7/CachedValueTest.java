package org.grp7;

import org.grp7.jvn.JvnException;
import org.grp7.jvn.JvnObject;
import org.grp7.jvn.JvnServerImpl;

import java.rmi.RemoteException;
import java.util.HashMap;
import java.util.Map;

public class CachedValueTest {
    private JvnServerImpl js;

    private Map<String, JvnObject> declaredObject = new HashMap<>();

    public CachedValueTest(Integer maxSize) {
        js = JvnServerImpl.jvnGetServer();
        js.setMaxCachedSize(maxSize);

        Runtime.getRuntime().addShutdownHook(new Thread(() -> {
            try {
                js.jvnTerminate();
            } catch (JvnException | RemoteException e) {
                e.printStackTrace();
            }
        }));
    }

    public void addNewObject(String name, Integer value) throws JvnException, RemoteException {
        // look up the IRC object in the JVN server
        // if not found, create it, and register it in the JVN server
        JvnObject jo = js.jvnLookupObject(name);

        if (jo == null) {
            jo = js.jvnCreateObject(value);
            // after creation, I have a write lock on the object
            jo.jvnUnLock();
            js.jvnRegisterObject(name, jo);
        }
        declaredObject.put(name, jo);
    }

    public JvnObject getObject(String name) {
        return declaredObject.get(name);
    }

    public static void main(String[] argv) throws RemoteException, JvnException {
        if (argv.length == 1) {
            try {
                int arg = Integer.parseInt(argv[0]);
                CachedValueTest test = new CachedValueTest(arg);

                System.out.println("Ajout Objet 1: ");
                test.addNewObject("test1", 1);
                System.out.println("Ajout Objet 2: ");
                test.addNewObject("test2", 2);
                System.out.println("Ajout Objet 3: ");
                test.addNewObject("test3", 3);
                System.out.println("Ajout Objet 4: ");
                test.addNewObject("test4", 4);

                System.out.println("Action sur l'objet 2: ");
                test.getObject("test2").jvnLockRead();
                float result = ((Integer) test.getObject("test2").jvnGetSharedObject()).floatValue();
                System.out.println("resultat en float du test2: " + result + "\n");
                test.getObject("test2").jvnUnLock();

                System.out.println("Ajout Objet 5: ");
                test.addNewObject("test5", 5);

                System.out.println("Action sur l'objet 1: ");
                test.getObject("test1").jvnLockRead();
                result = ((Integer) test.getObject("test1").jvnGetSharedObject()).floatValue();
                System.out.println("resultat en float du test1: " + result + "\n");
                test.getObject("test1").jvnUnLock();

                System.exit(0);
            } catch (NumberFormatException | InterruptedException nfe) {
                System.out.println("L'argument que vous avez spécifié n'est pas un nombre.");
            }
        } else {
            System.out.println("L'application doit avoir un nombre en argument (la taille du cache).");
        }
    }
}
