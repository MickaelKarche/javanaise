#!/bin/bash
if [ $# -ne 1 ]
then
  echo "Usage : ./stressTest-run [nombre de clients]"
  exit
fi
mvn package
cd target

osascript -e 'tell application "Terminal" to do script "cd tmp/sysRepartis/javanaise/target; java -jar Coordinator.jar &"'

sleep 5

counter=1
while [ $counter -le $1 ]
do
  osascript -e 'tell application "Terminal" to do script "cd tmp/sysRepartis/javanaise/target; java -jar Irc.jar 1 3 &"'
#  pid=$!
  sleep 0.1
  ((counter++))
done

#wait $pid
