Members
=================
KARCHE Mickael
GAUTRON Mathieu 
RIGOT Florent

Installation
-----------------

Compiler le projet

We use maven to build our projet.

        mvn clean package
    
We got Three JAR in the target/ directory

        CachedValueTest.jar
        Coordinator.jar
        Irc.jar

Note: JvnCoord will create files ending with .coor. Delete those if you want to remove all cache saves.

Usage
-----------------

First change directory to the target folder and launch the coordinator : `java -jar Coordinator.jar`

Then either launch Irc or CachedValueTest :

        Irc : java -jar Irc.jar <TestStatut> <nb_cachedsized>
        CachedValueTest : java -jar CachedValueTest.jar <nb_cachedsized>

Note: TestStatut should be set to either 0 or 1. 1 if you  want to run the stressTest and 0 if you don't want to.

You can also launch the script stressTest-run.sh from the root folder that is working on macOS.
For the stressTest-run.sh you have to specify the number of client you want to run:
        ./stressTest-run.sh <nb_client>

Extension
-----------------
1. Client Terminate (Specifies to the Coord that we are disconnected, so the rights on the objects are removed.)
2. Limited Cache Management (LRU, a test executable is provided)
3. Failure management on the coordinator side (with backup of the status on the coordinator side and reconnection on the client side)
4. Testing : StressTest & CacheTest